FROM ubuntu:22.10

LABEL maintainer="Flávio Lopes <flavio@links.inf.br"

ENV EMAIL flavio@links.inf.br

ENV VERSION 1.0

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get upgrade && apt-get install -qq vim wget curl unzip glibc-source groff less

WORKDIR /root
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN unzip awscliv2.zip
RUN ./aws/install
RUN rm awscliv2.zip

